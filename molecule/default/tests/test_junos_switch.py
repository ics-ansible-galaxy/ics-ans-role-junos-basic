import os
import testinfra.utils.ansible_runner


host = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-junos-basic-switch')


def test_config_file_exists(host):
    assert os.system('/config/juniper.conf.gz')


def test_default(host):
    assert os.popen("/usr/bin/zcat '/config/juniper.conf.gz' | grep -E 'ntp.*syslog.*archive'")
