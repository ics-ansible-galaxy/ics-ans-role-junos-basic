# ics-ans-role-junos-basic

Ansible role to install junos-basic.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
# Banner
junos_banner: "This is the property of European Spallation Source ERIC. Do not login without express permission."

# DNS
junos_basic_dns: true
junos_basic_nameservers:
  - 8.8.8.8
  - 8.8.4.4
junos_basic_domain: test.local.com
junos_basic_domains:
  - test.local.com
  - subdomain.local.com

# NTP server
junos_basic_ntp: true
junos_basic_ntpserver: 10.1.1.1

# Syslog
junos_basic_log: true
junos_basic_syslog: 10.1.1.2

# Archival server
junos_basic_archive: true
junos_basic_host_key: "ecdsa-sha2-nistp256-key AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOV6q2raNoQira9p6UZz2BEvjZEWcDsalU8qOofREJsoztzTi6N197l4RvJg4zRvmllEvCtzxx9h8zRcJxlEDRA="
junos_basic_fetch_from_server: 10.1.1.3
# One can use scp, sftp, ftp, http, pasvftp or file
# reference: https://www.juniper.net/documentation/en_US/junos/topics/reference/configuration-statement/archival-edit-system.html
junos_basic_archive_path: "scp://root@{{ junos_basic_fetch_from_server }}/patch/to/switch-backup/"
# vault this but should be in encryption method for idempotence
junos_basic_archive_password: "$9$-Gw4ZzF/uORHqtO1IrlgoJDqf69pEclp0RS"

# sflow
junos_basic_sflow: true
junos_basic_collector: 10.1.1.4
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-basic
```

## License

BSD 2-clause
